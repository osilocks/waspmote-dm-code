#include "WProgram.h"
void setup();
void loop();
int setNetworkParams();
packetXBee* paq_sent;
char*  data="Test message!";
uint8_t  PANID[2]={0x12,0x34};
char*  KEY="dgfc";
char* NODE = "DGFC-02";
long previous=0;
 
void setup() {
  // Inits the XBee DigiMesh library
  xbeeDM.init(DIGIMESH,FREQ2_4G,NORMAL);
  // Powers XBee
  xbeeDM.ON();
  int network = setNetworkParams();
  XBee.print("Network paramaters set: ");
  XBee.println(network);  
}

void loop() {
  XBee.println("Prepping Packet!");
  // Set params to send
  paq_sent=(packetXBee*) calloc(1,sizeof(packetXBee)); 
  paq_sent->mode=UNICAST;
  paq_sent->MY_known=0;
  paq_sent->packetID=0x52;
  paq_sent->opt=0; 
  xbeeDM.hops=0;
  xbeeDM.setOriginParams(paq_sent, "5678", MY_TYPE);
  xbeeDM.setDestinationParams(paq_sent, "DGFC-01", data, NI_TYPE, DATA_ABSOLUTE);
  xbeeDM.sendXBee(paq_sent);
  if( !xbeeDM.error_TX )
  {
    XBee.println("Sent Packet!");
  }
  free(paq_sent);
  paq_sent=NULL;
  
  XBee.println("Waiting for response");
  
  previous=millis();
  while( (millis()-previous) < 20000 )
  {
    if( XBee.available() )
    {
      xbeeDM.treatData();
      if( !xbeeDM.error_RX )
      {
        // Writing the parameters of the packet received
        while(xbeeDM.pos>0)
        {
          XBee.print("Network Address Source: ");
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->naS[0],HEX);
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->naS[1],HEX);
          XBee.println("");
          XBee.print("MAC Address Source: ");          
          for(int b=0;b<4;b++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->macSH[b],HEX);
          }
          for(int c=0;c<4;c++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->macSL[c],HEX);
          }
          XBee.println("");
          XBee.print("Network Address Origin: ");          
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->naO[0],HEX);
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->naO[1],HEX);
          XBee.println("");
          XBee.print("MAC Address Origin: ");          
          for(int d=0;d<4;d++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->macOH[d],HEX);
          }
          for(int e=0;e<4;e++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->macOL[e],HEX);
          }
          XBee.println("");
          XBee.print("RSSI: ");                    
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->RSSI,HEX);
          XBee.println("");         
          XBee.print("16B(0) or 64B(1): ");                    
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->mode,HEX);
          XBee.println("");
          XBee.print("Data: ");                    
          for(int f=0;f<xbeeDM.packet_finished[xbeeDM.pos-1]->data_length;f++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->data[f],BYTE);
          }
          XBee.println("");
          XBee.print("PacketID: ");                    
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->packetID,HEX);
          XBee.println("");      
          XBee.print("Type Source ID: ");                              
          XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->typeSourceID,HEX);
          XBee.println("");     
          XBee.print("Network Identifier Origin: ");          
          for(int g=0;g<4;g++)
          {
            XBee.print(xbeeDM.packet_finished[xbeeDM.pos-1]->niO[g],BYTE);
          }
          XBee.println("");  
          free(xbeeDM.packet_finished[xbeeDM.pos-1]);
          xbeeDM.packet_finished[xbeeDM.pos-1]=NULL;
          xbeeDM.pos--;
        }
        previous=millis();
      }
    }
  }
  delay(3000);
}

int setNetworkParams() {
  int success = 1;
  
  // Chosing a channel : channel 0x0D
  xbeeDM.setChannel(0x0D);
  if( !xbeeDM.error_AT ) {
    XBee.println("Channel set OK");
  } else {
    success = 0;
    XBee.println("Error while changing channel");
  }
  
  // Chosing a PANID : PANID=0x1234
  xbeeDM.setPAN(PANID);
  if( !xbeeDM.error_AT ) {
    XBee.println("PANID set OK");
  } else {
    success = 0;
    XBee.println("Error while changing PANID");  
  }
  
  // Enabling security : KEY="WaspmoteKey"
  xbeeDM.encryptionMode(1);
  if( !xbeeDM.error_AT ) {
    XBee.println("Security enabled");
  } else {
    success = 0;
    XBee.println("Error while enabling security");  
  }
  
  xbeeDM.setLinkKey(KEY);
  if( !xbeeDM.error_AT ) {
    XBee.println("Key set OK");
  } else {
    success = 0;
    XBee.println("Error while setting Key");  
  }
  
    // Set Node name
  xbeeDM.setNodeIdentifier(NODE); // Set name of OceanWasp01
  if( !xbeeDM.error_AT ) {
    XBee.println("Name set OK");
  } else {
    success = 0;
    XBee.println("Error while setting Node name");
  }
  
  // Keep values
  xbeeDM.writeValues();
  if( !xbeeDM.error_AT ) {
    XBee.println("Changes stored OK");
  } else {
    success = 0;
    XBee.println("Error while storing values");  
  }
  
  return success;
}
  

int main(void)
{
	init();

	setup();
    
	for (;;)
		loop();
        
	return 0;
}

