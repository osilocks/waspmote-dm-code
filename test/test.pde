uint8_t  PANID[2]={0x12,0x34};
long previous=0;
char*  KEY="dgfc";
char* NODE = "DGFC-01";
 
void setup() {
  // Inits the XBee DigiMesh library
  xbeeDM.init(DIGIMESH,FREQ2_4G,NORMAL);
  // Powers XBee
  xbeeDM.ON();
  int network = setNetworkParams();
  XBee.print("Network paramaters set: ");
  XBee.println(network);  
}

void loop() {
  XBee.println("Awaiting Message");
  previous=millis();
  
  while( (millis()-previous) < 20000 )
  {
    if( XBee.available() )
    {
      xbeeDM.treatData();
      if( !xbeeDM.error_RX )
      {
        XBee.println("Received something, no clue what!");
      }
    }
  }
  
  
  delay(3000);
}

int setNetworkParams() {
  int success = 0;
  
  // Chosing a channel : channel 0x0D
  xbeeDM.setChannel(0x0D);
  if( !xbeeDM.error_AT ) {
    success = 1;
    XBee.println("Channel set OK");
  } else {
    success = 0;
    XBee.println("Error while changing channel");  
  }
  
  // Chosing a PANID : PANID=0x1234
  xbeeDM.setPAN(PANID);
  if( !xbeeDM.error_AT ) {
    success = 1;
    XBee.println("PANID set OK");
  } else {
    success = 0;
    XBee.println("Error while changing PANID");  
  }
  
  // Enabling security : KEY="WaspmoteKey"
  xbeeDM.encryptionMode(1);
  if( !xbeeDM.error_AT ) {
    success = 1;
    XBee.println("Security enabled");
  } else {
    success = 0;
    XBee.println("Error while enabling security");  
  }
  
  xbeeDM.setLinkKey(KEY);
  if( !xbeeDM.error_AT ) {
    success = 1;
    XBee.println("Key set OK");
  } else {
    success = 0;
    XBee.println("Error while setting Key");  
  }
  
    // Set Node name
  xbeeDM.setNodeIdentifier(NODE); // Set name of OceanWasp01
  if( !xbeeDM.error_AT ) {
    success = 1;
    XBee.println("Name set OK");
  } else {
    success = 0;
    XBee.println("Error while setting Node name");
  }
  
  // Keep values
  xbeeDM.writeValues();
  if( !xbeeDM.error_AT ) {
    success = 1;
    XBee.println("Changes stored OK");
  } else {
    success = 0;
    XBee.println("Error while storing values");  
  }
  
  return success;
}
  
